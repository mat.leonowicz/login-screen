import React from 'react';
import useLocalStorage from 'react-use/lib/useLocalStorage';
import { ThemeProvider } from 'styled-components';
import PageContainer from './components/PageContainer';
import GlobalStyles from './GlobalStyles';
import theme from './style/theme';
import { LocalStorageKey } from './utils/config';
import Login from './views/Login/Login';
import MainContent from './views/MainContent';

function App() {
  const [authToken, setAuthToken] = useLocalStorage(LocalStorageKey.AUTH_TOKEN);
  return (
    <>
      <ThemeProvider theme={theme}>
        <PageContainer>
          {authToken ? (
            <MainContent logout={() => setAuthToken(null)} />
          ) : (
            <Login setAuthToken={setAuthToken} />
          )}
        </PageContainer>
      </ThemeProvider>
      <GlobalStyles />
    </>
  );
}

export default App;
