import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';
import theme from '../../style/theme';
import { PTresponsive } from '../../utils/propTypes';
import { styleCss } from '../../utils/styledComponents';
import Block from './Block';

const Text = styled(Block.withComponent('span'))`
  display: block;
  ${props => props.inline && css`
    display: inline-block;
  `};
  ${styleCss('color')};
  ${styleCss('fontFamily')};
  ${styleCss('fontSize')};
  ${styleCss('fontWeight')};
  ${styleCss('textAlign')};
`;
Text.defaultProps = {
  color: theme.color.fontBase,
  fontSize: theme.font.size.default,
};
Text.propTypes = {
  ...Block.propTypes,
  inline: PropTypes.bool,
  fontFamily: PropTypes.string,
  fontSize: PTresponsive(PropTypes.string),
  fontWeight: PTresponsive(PropTypes.number),
  textAlign: PTresponsive(PropTypes.oneOf(['center', 'start', 'end'])),
  color: PropTypes.string,
};
export default Text;
