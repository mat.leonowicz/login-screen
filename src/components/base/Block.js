import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';
import { PTresponsive, PTspacing } from '../../utils/propTypes';
import { styleCss, styleCssSpacing } from '../../utils/styledComponents';

const Block = styled.div`
  display: block; //setting this explicite helps with extending or usage with "as" attribute
  ${styleCssSpacing('height')};
  ${styleCssSpacing('minHeight')};
  ${styleCssSpacing('maxHeight')};
  ${styleCssSpacing('width')};
  ${styleCssSpacing('minWidth')};
  ${styleCssSpacing('maxWidth')};
  
  ${styleCssSpacing('margin')};
  ${styleCssSpacing('marginTop')};
  ${styleCssSpacing('marginBottom')};
  ${styleCssSpacing('marginLeft')};
  ${styleCssSpacing('marginRight')};
  ${styleCssSpacing('marginH', 'margin-left')};
  ${styleCssSpacing('marginH', 'margin-right')};
  ${styleCssSpacing('marginV', 'margin-top')};
  ${styleCssSpacing('marginV', 'margin-bottom')};
  
  ${styleCssSpacing('padding')};
  ${styleCssSpacing('paddingTop')};
  ${styleCssSpacing('paddingBottom')};
  ${styleCssSpacing('paddingLeft')};
  ${styleCssSpacing('paddingRight')};
  ${styleCssSpacing('paddingH', 'padding-left')};
  ${styleCssSpacing('paddingH', 'padding-right')};
  ${styleCssSpacing('paddingV', 'padding-top')};
  ${styleCssSpacing('paddingV', 'padding-bottom')};
  
  ${styleCss('bg', 'background')};
  ${styleCss('border')};
  ${styleCss('borderRadius')};
  
  ${props => props.relative && css`
    position: relative;
  `};
  
  ${props => props.clickable && css`
    &:hover {
      cursor: pointer;
    }
  `};
`;
Block.propTypes = {
  height: PTspacing,
  minHeight: PTspacing,
  maxHeight: PTspacing,
  width: PTspacing,
  maxWidth: PTspacing,
  margin: PTspacing,
  marginTop: PTspacing,
  marginBottom: PTspacing,
  marginLeft: PTspacing,
  marginRight: PTspacing,
  marginH: PTspacing,
  marginV: PTspacing,
  padding: PTspacing,
  paddingTop: PTspacing,
  paddingBottom: PTspacing,
  paddingLeft: PTspacing,
  paddingRight: PTspacing,
  paddingH: PTspacing,
  paddingV: PTspacing,
  bg: PTresponsive(PropTypes.string),
  border: PTresponsive(PropTypes.string),
  borderRadius: PTresponsive(PropTypes.string),
  relative: PropTypes.bool,
  clickable: PropTypes.bool,
};

export default Block;
