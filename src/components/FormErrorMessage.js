import React from 'react';
import theme from '../style/theme';
import { PTchildren } from '../utils/propTypes';
import Block from './base/Block';
import Text from './base/Text';

const FormErrorMessage = ({ children, ...props }) => (
  <Block
    paddingV={0.5}
    paddingH={2}
    borderRadius={theme.defaultBorderRadius}
    border={`1px solid ${theme.color.formErrorFont}`}
    bg={theme.color.formErrorBackground}
    {...props}
  >
    <Text color={theme.color.formErrorFont}>
      {children}
    </Text>
  </Block>
);

FormErrorMessage.propTypes = {
  ...Block.propTypes,
  children: PTchildren,
};

FormErrorMessage.defaultProps = {
  // eslint-disable-next-line react/default-props-match-prop-types
  marginBottom: 4,
};

export default FormErrorMessage;
