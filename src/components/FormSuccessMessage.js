import React from 'react';
import theme from '../style/theme';
import { PTchildren } from '../utils/propTypes';
import Block from './base/Block';
import Text from './base/Text';

const FormSuccessMessage = ({ children, ...props }) => (
  <Block
    paddingV={0.5}
    paddingH={2}
    borderRadius={theme.defaultBorderRadius}
    border={`1px solid ${theme.color.formSuccessFont}`}
    bg={theme.color.formSuccessBackground}
    {...props}
  >
    <Text color={theme.color.formSuccessFont}>
      {children}
    </Text>
  </Block>
);

FormSuccessMessage.propTypes = {
  ...Block.propTypes,
  children: PTchildren,
};

FormSuccessMessage.defaultProps = {
  // eslint-disable-next-line react/default-props-match-prop-types
  marginBottom: 4,
};

export default FormSuccessMessage;
