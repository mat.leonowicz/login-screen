import React from 'react';
import theme from '../style/theme';
import { PTchildren } from '../utils/propTypes';
import Block from './base/Block';

const Panel = ({ children, ...props }) => (
  <Block
    bg={theme.color.panelBg}
    borderRadius={theme.defaultBorderRadius}
    padding={[8, 12]}
    paddingV={[4, 6]}
    {...props}
  >
    {children}
  </Block>
);

Panel.propTypes = {
  children: PTchildren,
  ...Block.propTypes,
};

export default Panel;
