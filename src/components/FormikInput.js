import { ErrorMessage, Field } from 'formik';
import PropTypes from 'prop-types';
import React, { useMemo } from 'react';
import uuid from 'uuid/v4';
import Block from './base/Block';
import Input from './Input';
import Label from './Label';
import ValidationError from './ValidationError';

const FormikInput = ({ id: providedId, name, label, ...props }) => {
  const id = useMemo(() => providedId || uuid(), [providedId]);
  return (
    <Block marginBottom={3}>
      {label && <Label htmlFor={id} marginBottom={1}>{label}</Label>}
      <Field name={name}>
        {({ field }) => (
          <Input
            id={id}
            {...field}
            {...props}
          />
        )}
      </Field>
      <ErrorMessage name={name}>
        {msg => <ValidationError>{msg}</ValidationError>}
      </ErrorMessage>
    </Block>
  );
};

FormikInput.propTypes = {
  id: PropTypes.string,
  label: PropTypes.string,
  ...Input.propTypes,
};

export default FormikInput;
