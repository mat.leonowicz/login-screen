import styled from 'styled-components';
import { su } from '../utils/styledComponents';

const PageContainer = styled.div`
  padding: 0 ${su(4)};
  @media (min-width: ${props => props.theme.breakpoints.desktop}) {
    padding: 0 ${su(8)};
  }
  @media (min-width: ${props => props.theme.breakpoints.desktop}) {
    max-width: 1400px;
  }
  
  margin: 0 auto;
`;

export default PageContainer;
