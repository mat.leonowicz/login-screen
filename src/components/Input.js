import styled from 'styled-components';
import { su } from '../utils/styledComponents';
import Block from './base/Block';

const Input = styled(Block.withComponent('input'))`
  height: ${su(10)};
  padding: ${su(1)} ${su(3)};
  border-radius: ${props => props.theme.defaultBorderRadius};
  outline: none;
  border: 1px solid ${props => props.theme.color.border};
  &:focus {
    border: 1px solid ${props => props.theme.color.active};
  }
  &:disabled {
    border: 1px solid #a2a2b6;
    background: transparent;
  }
`;

Input.defaultProps = {
  width: '100%',
};

Input.propTypes = {
  ...Block.propTypes,
};

export default Input;
