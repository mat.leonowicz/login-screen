import React from 'react';
import theme from '../style/theme';
import { PTchildren } from '../utils/propTypes';
import Text from './base/Text';

const ValidationError = ({ children }) => (
  <Text color={theme.color.formErrorFont}>{children}</Text>
);

ValidationError.propTypes = {
  children: PTchildren,
};

export default ValidationError;
