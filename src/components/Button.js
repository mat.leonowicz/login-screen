import { darken, desaturate, transparentize } from 'polished';
import styled from 'styled-components';
import handleEnter from '../utils/handleEnter';
import { su } from '../utils/styledComponents';
import Block from './base/Block';

const Button = styled(Block.withComponent('button')).attrs(props => ({
  tabIndex: '0',
  ...(props.onClick ? { onKeyDown: handleEnter(props.onClick) } : {}),
  ...(props.disabled ? { onClick: undefined, onKeyDown: undefined } : {}),
}))`
  height: ${su(10)};
  padding: ${su(1)} ${su(3)};
  border: none;
  border-radius: ${props => props.theme.defaultBorderRadius};
  background: ${props => props.theme.color.buttonBg};
  color: ${props => props.theme.color.buttonFont};
  font-weight: ${props => props.theme.font.weight.bold};
  cursor: pointer;
  &:focus, &:hover {
    background: ${props => darken(0.04, props.theme.color.buttonBg)};
  }
  &:disabled {
    background: ${props => transparentize(0.4, desaturate(0.1, props.theme.color.buttonBg))};
  }
`;

Button.propTypes = {
  ...Block.propTypes,
};

export default Button;
