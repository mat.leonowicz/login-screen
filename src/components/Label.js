import styled from 'styled-components';
import Block from './base/Block';

const Label = styled(Block.withComponent('label'))``;

Label.propTypes = {
  ...Block.propTypes,
};

export default Label;
