import round from 'lodash/round';

export const pxToEm = value => `${round(value / 16, 3)}em`;
