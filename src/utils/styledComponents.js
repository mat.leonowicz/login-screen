import { css } from 'styled-components';

/**
 * Helper functions for easier styled-components creation.
 */

/** camelCaseToDash('marginBottom') = 'margin-bottom' */
function camelCaseToDash(str) {
  return str.replace(/([a-zA-Z])(?=[A-Z])/g, '$1-').toLowerCase();
}

/**
 * for responsive aware styling
 * arrays represent values at breakpoints
 * */
export const styleCss = (propName, cssPropName) =>
  props => Array.isArray(props[propName]) ? (
    css`
      ${cssPropName || camelCaseToDash(propName)}: ${props[propName][0]};
      
      ${props[propName].slice(1).map((value, index) => css`
        @media(min-width: ${props.theme.breakpoints[index]}) {
          ${cssPropName || camelCaseToDash(propName)}: ${value};
        }
      `)};
    `
  ) : (
    hasValueProvided(props[propName]) && css`
      ${cssPropName || camelCaseToDash(propName)}: ${props[propName]};
    `
  );

/**
 * for responsive and spacing unit aware styling
 * numbers are treated as spacing units
 * arrays represent values at breakpoints
 * */
export const styleCssSpacing = (propName, cssPropName) =>
  props => Array.isArray(props[propName]) ? (
    css`
      ${cssPropName || camelCaseToDash(propName)}: ${su(props[propName][0])};
      
      ${props[propName].slice(1).map((value, index) => css`
        @media(min-width: ${props.theme.breakpoints[index]}) {
          ${cssPropName || camelCaseToDash(propName)}: ${su(value)};
        }
      `)};
    `
  ) : (
    hasValueProvided(props[propName]) && css`
      ${cssPropName || camelCaseToDash(propName)}: ${su(props[propName])};
    `
  );

/** su stands for for spacing units */
export const su = (value) => {
  if (typeof value === 'number') {
    return props => `${value * props.theme.spacingUnit}px`;
  }
  return value;
};

const hasValueProvided = value => value || value === 0;
