import { render } from '@testing-library/react';
import React from 'react';
import { ThemeProvider } from 'styled-components';
import theme from '../style/theme';
import { PTchildren } from '../utils/propTypes';

const Providers = ({ children }) => (
  <ThemeProvider theme={theme}>
    {children}
  </ThemeProvider>
);

Providers.propTypes = {
  children: PTchildren,
};

const renderWithTheme = (ui, options) => render(ui, { wrapper: Providers, ...options });

export default renderWithTheme;
