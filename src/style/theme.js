import { pxToEm } from '../utils/pxToEm';

// Media queries in ems, see https://zellwk.com/blog/media-query-units/
const Breakpoints = {
  tablet: pxToEm(640),
  desktop: pxToEm(1280),
};

const theme = {
  spacingUnit: 4, //px
  breakpoints: [
    Breakpoints.tablet,
    Breakpoints.desktop,
  ],
  defaultBorderRadius: '5px',
  color: {
    fontBase: '#302f40',
    active: '#9467ff',
    border: '#e3e3e9',
    buttonBg: '#ff6f61',
    buttonFont: '#ffffff',
    panelBg: '#f7f7ff',
    formErrorFont: '#e01a11',
    formErrorBackground: '#ffe2e2',
    formSuccessFont: '#13e02d',
    formSuccessBackground: '#e5ffe5',
  },
  font: {
    size: {
      default: '16px',
    },
    weight: {
      normal: 400,
      medium: 500,
      bold: 700,
    },
  },
};

export default theme;
