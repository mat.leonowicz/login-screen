import { fireEvent, wait, waitForElementToBeRemoved } from '@testing-library/react';
import axios from 'axios';
import React from 'react';
import renderWithTheme from '../../testUtils/renderWithTheme';
import Login from './Login';

const TEST_ERROR = 'testError';
const TEST_TOKEN = 'testToken';
jest.mock('axios');

it('calls setAuthToken and shows message on successful login', async () => {
  axios.post.mockImplementationOnce(() => Promise.resolve({ status: 200, data: { authToken: TEST_TOKEN } }));
  const setAuthToken = jest.fn();

  const { getByText, getByLabelText, findByText } = renderWithTheme(<Login setAuthToken={setAuthToken} />);
  const email = getByLabelText('Email');
  const password = getByLabelText('Password');

  fireEvent.change(email, { target: { value: 'test@test.pl' } });
  fireEvent.change(password, { target: { value: 'Password1' } });
  fireEvent.click(getByText('Login'));

  expect(await findByText('Login successful')).toBeInTheDocument();
  await wait(() => {
    expect(setAuthToken).toHaveBeenCalledWith(TEST_TOKEN);
    expect(setAuthToken).toHaveBeenCalledTimes(1);
  });
});

it('shows error message on failed login', async () => {
  // eslint-disable-next-line prefer-promise-reject-errors
  axios.post.mockImplementationOnce(() => Promise.reject({ response: { status: 401, data: { error: TEST_ERROR } } }));
  const setAuthToken = jest.fn();

  const { getByText, getByLabelText, findByText } = renderWithTheme(<Login setAuthToken={setAuthToken} />);
  const email = getByLabelText('Email');
  const password = getByLabelText('Password');

  fireEvent.change(email, { target: { value: 'a@a.pl' } });
  fireEvent.change(password, { target: { value: 'ValidButNotCorrect1' } });
  fireEvent.click(getByText('Login'));

  expect(await findByText(TEST_ERROR)).toBeInTheDocument();
  await wait(() => {
    expect(setAuthToken).not.toHaveBeenCalled();
  });
});

it('shows client side validation for Email', async () => {
  const setAuthToken = jest.fn();
  const { getByText, getByLabelText, queryByText } = renderWithTheme(<Login setAuthToken={setAuthToken} />);

  const email = getByLabelText('Email');

  expect(queryByText('Email is required')).not.toBeInTheDocument();
  expect(queryByText('Invalid email')).not.toBeInTheDocument();

  fireEvent.click(email);
  fireEvent.blur(email);
  await wait(() => {
    expect(getByText('Email is required')).toBeInTheDocument();
  });

  fireEvent.change(email, { target: { value: 'a' } });
  await wait(() => {
    expect(getByText('Invalid email')).toBeInTheDocument();
  });

  fireEvent.change(email, { target: { value: 'a@a.pl' } });
  await waitForElementToBeRemoved(() => getByText('Invalid email'));
});

it('shows client side validation for Password', async () => {
  const setAuthToken = jest.fn();
  const { getByText, getByLabelText, queryByText } = renderWithTheme(<Login setAuthToken={setAuthToken} />);

  const password = getByLabelText('Password');

  expect(queryByText('Password is required')).not.toBeInTheDocument();
  expect(queryByText('Invalid password')).not.toBeInTheDocument();

  fireEvent.click(password);
  fireEvent.blur(password);
  await wait(() => {
    expect(getByText('Password is required')).toBeInTheDocument();
  });

  fireEvent.change(password, { target: { value: 'a' } });
  await wait(() => {
    expect(getByText('Invalid password')).toBeInTheDocument();
  });

  fireEvent.change(password, { target: { value: 'Password1' } });
  await waitForElementToBeRemoved(() => getByText('Invalid password'));
});
