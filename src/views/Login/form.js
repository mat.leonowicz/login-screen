import * as yup from 'yup';

// at least one digit, at least one lowercase letter, at least one uppercase letter, min 6 characters overall
const PASSWORD_REGEX = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}$/;

export const initialValues = {
  email: '',
  password: '',
  remember: false,
};

export const validationSchema = yup.object({
  email: yup.string()
    .email('Invalid email')
    .required('Email is required'),
  password: yup.string()
    .required('Password is required')
    .test(
      'password',
      'Invalid password',
      value => (value || '').match(PASSWORD_REGEX),
    ),
});
