import { Formik } from 'formik';
import get from 'lodash/get';
import PropTypes from 'prop-types';
import React, { useCallback, useState } from 'react';
import { login } from '../../api';
import Block from '../../components/base/Block';
import Flex from '../../components/base/Flex';
import Button from '../../components/Button';
import FormErrorMessage from '../../components/FormErrorMessage';
import FormikInput from '../../components/FormikInput';
import FormSuccessMessage from '../../components/FormSuccessMessage';
import Label from '../../components/Label';
import Panel from '../../components/Panel';
import { initialValues, validationSchema } from './form';

const Login = ({ setAuthToken }) => {
  const [serverError, setServerError] = useState(null);
  const [serverSuccess, setServerSuccess] = useState(false);

  const onSubmit = useCallback(async (values, { setSubmitting }) => {
    try {
      const response = await login(values);
      setServerError(null);
      setServerSuccess(true);
      setTimeout(() => {
        setAuthToken(response.data.authToken);
      }, 1000);
    } catch (e) {
      if (get(e, 'response.data.error')) {
        setServerError(e.response.data.error);
      }
    }
    setSubmitting(false);
  }, [setAuthToken]);

  return (
    <Flex as="section" justifyContent="center" paddingV={10}>
      <Panel width="100%" maxWidth={['400px', '500px']}>
        <Formik
          isInitialValid
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={onSubmit}
        >
          {({ touched, errors, handleSubmit, isSubmitting }) => (
            <form onSubmit={handleSubmit}>
              <Block as="fieldset">
                <FormikInput
                  label="Email"
                  id="email"
                  type="text"
                  name="email"
                />
                <FormikInput
                  label="Password"
                  id="password"
                  type="password"
                  name="password"
                />
                <Flex alignItems="center" marginBottom={3}>
                  <Label htmlFor="remember" marginRight={2}>Remember me</Label>
                  <input type="checkbox" name="remember" id="remember" />
                </Flex>
                {serverError && <FormErrorMessage>{serverError}</FormErrorMessage>}
                {serverSuccess && <FormSuccessMessage>Login successful</FormSuccessMessage>}
                <Button
                  type="submit"
                  width="100%"
                  disabled={isSubmitting || (Object.keys(touched).length > 0 && Object.keys(errors).length > 0)}
                >
                  Login
                </Button>
              </Block>
            </form>
          )}
        </Formik>
      </Panel>
    </Flex>
  );
};

Login.propTypes = {
  setAuthToken: PropTypes.func.isRequired,
};

export default Login;
