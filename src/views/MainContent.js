import PropTypes from 'prop-types';
import React from 'react';
import Block from '../components/base/Block';
import Flex from '../components/base/Flex';
import Button from '../components/Button';

const MainContent = ({ logout }) => (
  <Flex column alignItems="center">
    <Block marginBottom={4}>Here would be the main content</Block>
    <Button onClick={logout}>Logout</Button>
  </Flex>
);

MainContent.propTypes = {
  logout: PropTypes.func.isRequired,
};

export default MainContent;
