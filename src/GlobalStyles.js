import { createGlobalStyle } from 'styled-components';
import styledNormalize from 'styled-normalize';
import theme from './style/theme';

const GlobalStyles = createGlobalStyle`
  ${styledNormalize};
  
  html {
    box-sizing: border-box;
  }
  
  * {
    &,
    &:before,
    &:after {
      box-sizing: inherit;
    }
  }
  
  body {
    margin: 0;
    font-family: "Roboto", sans-serif;
    line-height: 1.5;
    color: ${theme.color.fontBase};
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }
  
  fieldset {
    border: 0;
    padding: 0.01em 0 0 0;
    margin: 0;
    min-width: 0;
  }
  body:not(:-moz-handler-blocked) fieldset {
      display: table-cell;
  }
`;

export default GlobalStyles;
