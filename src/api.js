import axios from 'axios';

export const login = data => axios.post('/login', data);

if (process.env.REACT_APP_ENV === 'test') {
  const MockAdapter = require('axios-mock-adapter');
  const mock = new MockAdapter(axios);
  mock.onPost('/login').reply(function (config) {
    const data = JSON.parse(config.data);

    if (data.email === 'test@test.pl' && data.password === 'Password1') {
      return [200, {
        authToken: 'testToken',
      }];
    }
    return [401, {
      error: 'invalid email or password',
    }];
  });
}
