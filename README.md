Notes:

- This project uses styled-components. Make sure you have an extension for it installed in your IDE
- I'm using my own kind of system for basic building blocks, to make development more consistent. Components such as Block, Flex and Text have various props that can accept props in form of a number (which would be treated as 'spacing unit', or string which would be used directly, or array of the former, which would result in using certain values at various breakpoints. Spacing unit is used for better consistency on UI.
- At app runtime, API is mocked when REACT_APP_ENV is set to 'test' - which it is, when you run `start` script to be able to play around. Tests do not rely on this behavior. They mock api themselves.
- React with alpha version is used because of problems with act(), see https://github.com/testing-library/react-testing-library/issues/281#issuecomment-480349552
